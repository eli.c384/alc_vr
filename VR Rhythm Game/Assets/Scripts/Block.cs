using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BlockColor
{
    A,
    B
}

public class Block : MonoBehaviour
{
    public BlockColor color;
    
    public GameObject brokenBlockLeft;
    public GameObject brokenBlockRight;
    public float brokenBlockForce;
    public float brokenBlockTorque;
    public float brokenBlockDestroyDelay;
    public ParticleSystem blockBreakParticle;
    
//    [SerializeField] private Material myMaterial;
//    [SerializeField] private Renderer myModel;
//    
//    
//    void Start()
//    {
//        Color color = myModel.material.color;
//        color.a = .5f;
//        myModel.material.color = color;
//    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("SwordA"))
        {
            if(color == BlockColor.A && GameManager.instance.leftSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            
            Hit();
        }
        else if(other.CompareTag("SwordB"))
        {
            if(color == BlockColor.B && GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            
            Hit();
        }
    }
    
//    void Transparency(Collider other)
//    {
//        if(other.CompareTag("TransparencyDetector"))
//        {
//            
//        }
//    }
    
     public void Hit ()
    {
        // enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);
        
        // enable particles
        blockBreakParticle.gameObject.SetActive(true);
        blockBreakParticle.Clear();
        blockBreakParticle.Play();

        
        // remove them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;
        blockBreakParticle.transform.parent = null;
        
        // add force to them
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
        
        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
        
        //add torque to them
        leftRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockTorque, ForceMode.Impulse);
        
        // destroy the broken piecesc after a few sedconds
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);
        Destroy(blockBreakParticle, brokenBlockDestroyDelay);
        
        // destory the main block
        Destroy(gameObject);
    }
}
